package br.edu.ifpb.caju.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Reuniao {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private LocalDate dataAgenda;
	private LocalDate dataReunicao;
	private Membro presenca;
	private int periodo;
	private String pauta;
	private Ata ata;
	
	public Reuniao(){}
	
	public LocalDate getDataAgenda() {
		return dataAgenda;
	}
	public void setDataAgenda(LocalDate dataAgenda) {
		this.dataAgenda = dataAgenda;
	}
	public LocalDate getDataReunicao() {
		return dataReunicao;
	}
	public void setDataReunicao(LocalDate dataReunicao) {
		this.dataReunicao = dataReunicao;
	}
	public Membro getPresenca() {
		return presenca;
	}
	public void setPresenca(Membro presenca) {
		this.presenca = presenca;
	}
	public int getPeriodo() {
		return periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public String getPauta() {
		return pauta;
	}
	public void setPauta(String pauta) {
		this.pauta = pauta;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Ata getAta() {
		return ata;
	}
	public void setAta(Ata ata) {
		this.ata = ata;
	}
	
	
	
}
