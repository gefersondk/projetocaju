package br.edu.ifpb.caju.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Colegiado {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private LocalDate dataIni;
	private LocalDate dataFim;
	private ArrayList<Membro> membros;
	private Membro presidente;
	private boolean ativo;
	
	public Colegiado(){
		
	}

	public LocalDate getDataIni() {
		return dataIni;
	}
	
	public String getDataFormatada(LocalDate data){
		
		DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return data.format(formatador);
	}

	public void setDataIni(LocalDate dataIni) {
		this.dataIni = dataIni;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	public ArrayList<Membro> getMembros() {
		return membros;
	}

	public void setMembros(ArrayList<Membro> membros) {
		this.membros = membros;
	}
	
	public void setMembro(Membro membro){
		this.membros.add(membro);
	}

	public Membro getPresidente() {
		return presidente;
	}

	public void setPresidente(Membro presidente) {
		this.presidente = presidente;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
