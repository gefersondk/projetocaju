package br.edu.ifpb.caju.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Voto {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String decisao;
	private String texto;
	
	public Voto(){}
	
	public Voto(String decicao,String texto){
		this.decisao = decicao;
		this.texto = texto;
	}

	public String getDecisao() {
		return decisao;
	}

	public void setDecisao(String decisao) {
		this.decisao = decisao;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
