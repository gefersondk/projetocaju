package br.edu.ifpb.caju.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Processo {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int idProcesso;
	private int matRequerente;
	private String nomeRequerente;
	private LocalDate dataDoc;
	private String assunto;
	private String periodo;
	
	public Processo(){}
	
	public int getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}

	public int getMatRequerente() {
		return matRequerente;
	}

	public void setMatRequerente(int matRequerente) {
		this.matRequerente = matRequerente;
	}

	public String getNomeRequerente() {
		return nomeRequerente;
	}

	public void setNomeRequerente(String nomeRequerente) {
		this.nomeRequerente = nomeRequerente;
	}

	public LocalDate getDataDoc() {
		return dataDoc;
	}

	public void setDataDoc(LocalDate dataDoc) {
		this.dataDoc = dataDoc;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
